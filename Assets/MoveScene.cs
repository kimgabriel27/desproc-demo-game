﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            DialogueSystem.instance.player.canMove = false;
            DialogueSystem.instance.StartDialogue(0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextScene(int index)
    {
        SceneManager.LoadScene(index);
    }
}
