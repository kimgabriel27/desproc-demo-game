﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Patient : MonoBehaviour
{
    public float speed;
    public float timer;

    private bool isMoving = false;
    private bool isGoingHome = false;

    // Start is called before the first frame update
    void Start()
    {
        speed = Random.Range(2f, 4f);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isMoving)
            timer -= Time.deltaTime;
        if (!isMoving && timer <= 0)
        {
            isMoving = true;
            timer = 0;

        }
        if (isMoving)
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(Random.Range(-1.5f, 4.5f), -1.2f), speed * Time.deltaTime);
        if (isGoingHome)
            timer -= Time.deltaTime;
        if (isGoingHome && timer <= 0)
            SceneManager.LoadScene(0);

    }

    public void Clicked()
    {
        if (!CompareTag("last"))
            transform.gameObject.SetActive(false);
        else
        Finished();
    }

    public void Finished()
    {
        transform.gameObject.GetComponent<SpriteRenderer>().color = new Color(0, 0, 1, 0);
        DialogueSystem.instance.StartDialogue(0);
        isGoingHome = true;
        timer = 5;
    }
}
