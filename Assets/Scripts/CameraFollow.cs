﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform target;


    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(target.position.x, 0f, 28.5f), Mathf.Clamp(target.position.y, 0, 0), transform.position.z);
    }
}
