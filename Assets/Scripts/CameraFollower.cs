﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    public GameObject player;
    public Vector2 cameraMax;
    public Vector2 cameraMin;
    public float dampTime;

    private Vector3 velocity;  
    // Start is called before the first frame update
    void Start()
    {
        velocity = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 playerPos = player.transform.position;
        Vector3 point = gameObject.GetComponent<Camera>().WorldToViewportPoint(playerPos);
        Vector3 delta = playerPos - gameObject.GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
        Vector3 destination = transform.position + delta;

        float destinationX = destination.x;
        float destinationY = destination.y;

        if (player.transform.position.x > cameraMax.x || player.transform.position.x < cameraMin.x)
            destinationX = transform.position.x;
        if (player.transform.position.y > cameraMax.y || player.transform.position.y < cameraMin.y)
            destinationY = transform.position.y;

        destination = new Vector3(destinationX, destinationY, destination.z);
        transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
    }
}
