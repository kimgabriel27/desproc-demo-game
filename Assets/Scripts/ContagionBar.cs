﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContagionBar : MonoBehaviour
{
    public Slider slider;
    public Gradient gradient;
    public Image fill;

    public void SetMaxContagion(float i)
    {
        slider.maxValue = i;
        slider.value = 20;

        fill.color = gradient.Evaluate(0f);
    }

    public void SetContagionLvl(float i)
    {
        slider.value = i;

        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
