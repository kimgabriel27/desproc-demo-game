﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdSpawner : MonoBehaviour
{
    public GameObject crowd;
    public float yPos;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnCrowd());
    }

    // Update is called once per frame
    void Update()
    {
        yPos = Random.Range(-4.4f, -1.5f);

    }
    IEnumerator SpawnCrowd()
    {
        while (true)
        {
            Instantiate(crowd, new Vector3(-9.5f, yPos, 0), Quaternion.identity);
            yield return new WaitForSeconds(3f);
        }
        
    }
}
