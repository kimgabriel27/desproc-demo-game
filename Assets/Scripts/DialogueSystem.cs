﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour
{
    public DialogueUIHandler UIHandler;
    public MouseMove player;

    public List<DialogueSequence> Sequences;
    private DialogueSequence CurrentSequence;
    private bool StartedDialogue = false;

    public static DialogueSystem instance;

    [HideInInspector] public UnityEvent OnDialogueEnd;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (!StartedDialogue) return;

        if (Input.GetKeyDown(KeyCode.Escape))
            OnDialogueDone();

        if (!Input.GetMouseButtonDown(0))   return;

        if (UIHandler.IsTyping)
        {
            Debug.Log("Skip");
            UIHandler.SkipTyping();
        }
        else if (UIHandler.CanTypeAgain)
        {
            Debug.Log("Next");
            NextLine();
        }
    }

    public void StartDialogue(int index)
    {
        if (player != null)
        {
            player.canMove = false;
            player.moving = false;
        }
        CurrentSequence = Sequences[index];
        CurrentSequence.ResetDialogue();

        UIHandler.ShowUIObject();
        UIHandler.StartTyping(CurrentSequence.CurrentDialogue.Name, CurrentSequence.CurrentDialogue.DialogueText);
        StartedDialogue = true;
    }
    

    // Call to read the Next line
    public void NextLine()
    {
        UIHandler.CanTypeAgain = false;
        // if the current dialogue is on its last line, remove all dialogue components
        if(CurrentSequence.IsDone)
        {
            OnDialogueDone();
            // End of Dialogue
        }
        else
        {
            // Changes the dialogue, and then changes the images, then starts typing the dialogue
            Dialogue next = CurrentSequence.NextDialogue();

            UIHandler.StartTyping(next.Name, next.DialogueText);
        }
    }

    private void OnDialogueDone()
    {
        UIHandler.HideUIObject();
        UIHandler.ClearDialogue();
        StartedDialogue = false;
        OnDialogueEnd.Invoke();

        if (player != null)
            player.canMove = true;
    }
}