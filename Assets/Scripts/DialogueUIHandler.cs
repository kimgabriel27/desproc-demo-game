﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class DialogueUIHandler : MonoBehaviour
{
    [SerializeField] Text DialogueName;
    [SerializeField] Text DialogueText;
    [Space(10)]

    [HideInInspector] public bool IsTyping = false;
    [HideInInspector] public bool CanTypeAgain = true;
    public GameObject dialogueBox;
    private Coroutine TypingCoroutine;
    private string currentDialogue = "";

    public void ShowUIObject()
    {
        dialogueBox.SetActive(true);
    }

    public void HideUIObject()
    {
        dialogueBox.SetActive(false);
    }

    // Call to start typing
    public void StartTyping(string name, string dialogue)
    {
        currentDialogue = dialogue;
        DialogueName.text = name;
        string color = "<color=#FFFFFF00>";
        DialogueText.text = color + dialogue + "</color>";
        IsTyping = true;
        TypingCoroutine = StartCoroutine(TypeDialogue(dialogue));
    }

    IEnumerator TypeDialogue(string dialogue)
    {
        CanTypeAgain = false;
        int current = 0;
        string color = "<color=#FFFFFF00>";
        while (current < dialogue.Length && IsTyping)
        {
            string start = dialogue.Remove(current);
            string end = dialogue.Remove(0, current);

            DialogueText.text = start + color + end + "</color>";
            current++;
            yield return null;
            yield return null;
        }
        DialogueText.text = dialogue;

        yield return new WaitForSeconds(.5f);
        CanTypeAgain = true;
        IsTyping = false;
    }

    // Call to skip tying
    public void SkipTyping()
    {
        StopCoroutine(TypingCoroutine);
        DialogueText.text = currentDialogue;
        CanTypeAgain = true;
        IsTyping = false;
    }

    // Call to clear Dialogue Box
    public void ClearDialogue()
    {
        if (TypingCoroutine != null) StopCoroutine(TypingCoroutine);

        DialogueName.text = "";
        DialogueText.text = "";
    }
}
