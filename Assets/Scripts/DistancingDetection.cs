﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistancingDetection : MonoBehaviour
{
    public Transform player;
    public GameObject people;
    public Text distanceText;

    public int peopleCount;
    public List<GameObject> groupOfPeople;

    public float xPos;
    public float yPos;
    // Start is called before the first frame update
    void Start()
    {
        
        groupOfPeople = new List<GameObject>();
     
    }

    // Update is called once per frame
    void Update()
    {

        foreach (var a in groupOfPeople)
        {
           
            if (Vector2.Distance(player.position, a.transform.position) < 5.0f)
            {
                Debug.Log("Closer");
               
            }
            else
            {
                distanceText.text = "";
            }
        }
     
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("People"))
        {
            Debug.Log("Too Close to the person");
            distanceText.text = "Too Close";
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("People"))
        {
            Debug.Log("Too Close to the person");
            distanceText.text = "Too Close";
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("People"))
        {
            Debug.Log("NO Person");
            distanceText.text = "";
        }
    }
}
