﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MouseMove : MonoBehaviour
{
    public float speed = 10f;
    Vector2 lastClickedPos;
    public bool canMove;


    public bool moving;

    void Start()
    {
        canMove = true;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && canMove)
        {
            lastClickedPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            moving = true;
        }
        if(moving && (Vector2)transform.position != lastClickedPos)
        {
            float step = speed * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, lastClickedPos, step);

        }
        else
        {
            moving = false; 
        }

        if (SceneManager.GetActiveScene().buildIndex == 3)
            canMove = false;
    }
}
