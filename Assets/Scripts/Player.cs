﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int currentProgress = 0;
    public float currentContagionLvl = 10f;
    public int maxProgress;


    public ProgressBar progressBar;
    public ContagionBar contagionBar;
        
    // Start is called before the first frame update
    void Start()
    {
        maxProgress = 100;
        progressBar.SetMaxProgressBar(maxProgress);
        contagionBar.SetMaxContagion(100);


    }
        
    // Update is called once per frame
    void Update()
    {
       
        if(Input.GetKeyDown(KeyCode.Space))
        {
            AddProgress(10);
        }
    }
    void AddProgress(int progress)
    {
        currentProgress += progress;

        progressBar.SetProgress(currentProgress);
    }
    void AdjustContagion(float contagionLvl)
    {
        currentContagionLvl += contagionLvl;
        contagionBar.SetMaxContagion(currentContagionLvl);

    }
}
